const WIDTH = 1024;
const HEIGHT = 576;
const SLICE = 10;
const PX_PER_SEC = 500;
const SHADOW_SIZE = 10;
const CTX = document.querySelector('#canvas').getContext('2d');
let CONTROL_RADIUS = 15;
const ControlCtx = document.querySelector('#controlCanvas').getContext('2d');
/**
 *
 * @type {HTMLAudioElement | null}
 */
const AUDIO = document.querySelector('#audio');
const PORT = 2229;
const JUDGE_LINE_POS = .88;
const ControlPointColor = {
    0: "#F00",
    1: "#00F",
    fill: "#FFF",
};

const LevelNameMapping = {
    0: 'easy',
    1: 'normal',
    2: 'hard',
};

let currentPos = [-1, -1];
let _songs = [];
let gameData = null;
let currentSelectIdx = -1;
let currentLevel = 1;
let currentSong = null;
let gameState = 0;
let _godMode = false;

function playEffect(src) {
    let audio = new Audio();
    audio.src = `./sound/${src}.ogg`;
    audio.volume = 1;
    audio.play();
}

function loadGameData() {
    return fetch("./assets/Deemo.csv")
        .then(x => x.text())
        .then(txt => {
            return csv.toObjects(txt);
        });
}

function initGame(songs) {
    gameState = 0;
    let songsEle = $('#songs');
    songsEle.empty();
    _songs = songs.map(song => {
        let div = document.createElement('div');
        div.classList.add('song');
        div.innerHTML = `
            <div class="song-inner">
                <div class="song-info">
                    <div class="song-name">${song.get('title')}</div>
                    <div class="song-level" data-easy="${song.get('easy')}" data-normal="${song.get('normal')}" data-hard="${song.get('hard')}"></div>
                </div>
                <div class="song-cover">
                    <img src="./assets/cover/${song.get('cover')}"/>
                </div>
                <audio hidden src="./assets/mp3/${song.get('audio')}" loop="loop"/>
            </div>
        `;

        let obj = {
            song,
            div,
            levelElement: div.querySelector('.song-level'),
            audio: div.querySelector('audio'),
        };

        // obj.audio.addEventListener('timeupdate', function () {
        //     if (this.currentTime > 60)
        //         this.currentTime = 0;
        // });

        return obj;
    });

    _songs.forEach(song => {
        songsEle.append(song.div);
    });

    $('.song .song-info').on('click', function (e) {
        currentLevel = (++currentLevel) % 3;
        songsEle.attr('data-level', LevelNameMapping[currentLevel]);
        playEffect('change-level');
        e.stopPropagation();
    });
    let startGamePanel = $("#confirmStart");
    songsEle.on('wheel', function (e) {
        e.preventDefault();
        e.stopPropagation();
        let dy = e.originalEvent.deltaY;
        if (dy > 0) {
            showSongByIdx(currentSelectIdx + 1);
        } else if (dy < 0) {
            showSongByIdx(currentSelectIdx - 1);
        }
    }).on('click', function (e) {
        startGamePanel.addClass('show');
        playEffect('selection');
    });

    startGamePanel.on('click', function (e) {
        e.stopPropagation();
        startGamePanel.removeClass('show');
        playEffect('cancel');
    });

    $("#startGame").on('click', function (e) {
        e.stopPropagation();
        startGame();
    });
    $('#nextGame').on('click', function (e) {
        e.stopPropagation();
        nextGame();
    });

    showSongByIdx(0);
}

function startGame() {
    currentSong = _songs[currentSelectIdx];
    currentSong.audio.pause();
    $("#initGameContainer").removeClass('show');
    $("#mainContainer").addClass('show');
    AUDIO.pause();
    AUDIO.src = currentSong.audio.src;
    AUDIO.currentTime = 0;
    CONTROL_RADIUS = 15;
    CTX.clearRect(0, 0, WIDTH, HEIGHT);
    ControlCtx.clearRect(0, 0, WIDTH, HEIGHT);
    if(gameLoop.__progressEle) gameLoop.__progressEle.textContent = '0.00%';

    $("#songName").text(currentSong.song.get('title'));
    let curSongInfoEle = $("#curSongInfo");
    curSongInfoEle.find('.songName').text(currentSong.song.get('title'));
    let levelTxt = LevelNameMapping[currentLevel];
    curSongInfoEle.find('.songLevel').text(`${levelTxt[0].toUpperCase() + levelTxt.substring(1)} LV${currentSong.song.get(levelTxt)}`);

    setGameMsg('Connecting to camera');
    testConnect().catch(e => {
        console.error(e);
        setGameMsg('Failed to connect to camera.', false);
        return Promise.reject(e);
    }).then((d) => {
        return Promise.all([
            processData(`./assets/json/${currentSong.song.get('json')}.${LevelNameMapping[currentLevel]}.json`),
            new Promise((a) => {
                setGameMsg('Ready?', false);
                setTimeout(() => a(), 3000);
            }),
        ]);
    }).then(dArr => {
        let c = dArr[0];
        setGameMsg(null);
        setCombo(0);
        gameData = c;
        gameData.scoreInfo = {
            hit: 0,
            miss: 0,
            combo: 0,
            maxCombo: 0,
            hitNodes: new Set(),
            processedNodes: new Set(),
        };
        gameState = 1;
        AUDIO.onended = function () {
            setTimeout(showResult, 2000);
        };
        AUDIO.play();
    })
}

let _gameMsgEle = $("#gameMsg");

function setGameMsg(txt, running = true) {
    if (txt === undefined || txt === null) {
        _gameMsgEle.hide();
    } else {
        _gameMsgEle.show();
        _gameMsgEle.find('.text').text(txt);
        if (running) {
            _gameMsgEle.find('.dot').show();
        } else {
            _gameMsgEle.find('.dot').hide();
        }
    }
}

let _moveTimer = null;

function showSongByIdx(i) {
    i = (i + _songs.length) % _songs.length;

    //clean up
    if (_songs[currentSelectIdx]) {
        _songs[currentSelectIdx].audio.pause();
    }
    if (_moveTimer !== null) {
        clearTimeout(_moveTimer);
        _moveTimer = null;
    }


    let y = -i * HEIGHT;
    let songsEle = $("#songs");
    songsEle.css('margin-top', y + 'px');

    _moveTimer = setTimeout(() => {
        currentSelectIdx = i;
        let a = _songs[i].audio;
        a.currentTime = isNaN(a.duration) ? 10 : a.duration * .2;
        a.play();
        _moveTimer = null;
    }, currentSelectIdx !== -1 ? 250 : 0);
}

function setCombo(num) {
    if (setCombo.__ele === undefined) {
        setCombo.__ele = document.querySelector('#comboEffect');
        setCombo.__ele_num = setCombo.__ele.querySelector('.num');
    }

    if (setCombo.__timer) clearTimeout(setCombo.__timer);
    if (setCombo.__num_timer) clearTimeout(setCombo.__num_timer);

    if (num < 5) {
        setCombo.__ele.classList.add('hide');
    } else {
        setCombo.__ele.classList.remove('hide');
        setCombo.__ele_num.textContent = num;
        setCombo.__ele_num.classList.add('changed');
        
        setCombo.__num_timer = setTimeout(() => {
            setCombo.__ele_num.classList.remove('changed');
        }, 50);

        setCombo.__timer = setTimeout(() => {
            setCombo.__ele.classList.add('hide');
        }, 500);
    }
}

function showResult() {
    if (gameState !== 1) return;
    $("#mainContainer").removeClass('show');
    let rc = $("#resultContainer").addClass('show');
    AUDIO.pause();
    gameState = 2;

    let judge = 'F';
    let score = getScore();
    if (score < 60) {
        judge = 'F';
    } else if (score <= 70) {
        judge = 'D';
    } else if (score <= 80) {
        judge = 'C';
    } else if (score <= 87) {
        judge = 'B';
    } else if (score <= 92) {
        judge = 'A';
    } else if (score < 95) {
        judge = 'AA';
    } else if (score < 100) {
        judge = 'AAA';
    } else if (score === 100) {
        judge = 'AAA';
    }
    
    rc.find('.level-img').attr('data-judge', judge);
    rc.find('.score').text(score.toFixed(2) + '%');
    rc.find('.comboText').text(gameData.scoreInfo.maxCombo);
}

function nextGame() {
    gameData = null;
    gameState = 0;
    $("#confirmStart").removeClass('show');
    $('#mainContainer, #resultContainer').removeClass('show');
    $('#initGameContainer').addClass('show');
    AUDIO.pause();
    showSongByIdx(currentSelectIdx);
}

function loadData(url) {
    return fetch(url).then(r => r.json());
}

function processData(url) {
    const BAR_WIDTH = 120;
    const BAR_HEIGHT = 10;
    return loadData(url).then(data => {
        let maxTime = Math.max(...data.notes.map(x => x._time || 0));
        let height = Math.ceil(maxTime) * PX_PER_SEC;

        let map = new Map();
        data.notes.filter(n => Math.abs(n.pos || 0) <= 2).forEach(n => {
            let id = parseInt(n.$id, 10);
            let time = n._time;
            let pos = n.pos || 0;
            let size = n.size || 1;
            map.set(id, {
                id, time, pos, size,
                type: "normal",
                group: -1,
            });
        });

        data.links.forEach((link, i) => {
            link.notes.map(n => parseInt(n.$ref, 10))
                .forEach(id => {
                    let obj = map.get(id);
                    if (obj === undefined) return;
                    obj.group = i;
                    obj.type = "keep";
                })
        });

        const HALF_BAR_WIDTH = BAR_WIDTH >> 1;
        const HALF_BAR_HEIGHT = BAR_HEIGHT >> 1;
        const barRange = {
            min: BAR_WIDTH * 2,
            max: WIDTH - BAR_WIDTH * 2,
        };
        barRange.diff = barRange.max - barRange.min;

        let canvasArr = [];
        let ctxArray = [];
        let notesBin = [];
        map.forEach((v, k) => {
            let i = Math.floor(v.time / SLICE);
            let ctx = ctxArray[i];
            if (ctx === undefined) {
                let c = document.createElement('canvas');
                c.width = WIDTH;
                c.height = SLICE * PX_PER_SEC;
                canvasArr[i] = c;
                ctx = c.getContext('2d');
                ctxArray[i] = ctx;
                let offsetY = -height + Math.floor(i * SLICE * PX_PER_SEC) + c.height;
                ctx.translate(0, offsetY);
                ctx.shadowBlur = SHADOW_SIZE;
                ctx.shadowColor = '#FFF';
                c._offsetY = offsetY;

                notesBin[i] = [];
            }

            let x = ((2 + v.pos) / 4) * barRange.diff + barRange.min;
            let y = height - Math.floor(v.time * PX_PER_SEC);
            let w = v.size * BAR_WIDTH;

            if (v.type === 'keep') {
                ctx.fillStyle = 'yellow';
            } else {
                ctx.fillStyle = '#333';
            }

            ctx.fillRect(x - HALF_BAR_WIDTH, y - HALF_BAR_HEIGHT, w, BAR_HEIGHT);

            v._drawInfo = {
                left: x - HALF_BAR_WIDTH,
                top: y - HALF_BAR_HEIGHT,
                width: w,
                height: BAR_HEIGHT,
                idx: i,
            };
            notesBin[i].push(k);
        });

        return {canvasArr, height, map, notesBin};
    });
}

function connectToServer() {
    return new Promise((a, b) => {
        let wsPath = `ws://localhost:${PORT}/echo`;
        if(location.protocol==='https:') {
            wsPath = 'wss://' + (function(){let s = location.origin;return s.substr(s.indexOf('//')+2)})() 
                    + (function(){let s = location.pathname;return s.substring(0, s.lastIndexOf('/')+1)})() + "control";
        }
        let ws = new WebSocket(wsPath);

        ws.onopen = function () {
            a(ws);
        };

        ws.onerror = function (e) {
            b(e);
        }
    });
}

function gameLoop() {
    if (gameState === 1) {

        if (gameLoop.__progressEle === undefined) gameLoop.__progressEle = document.querySelector("#curSongInfo .progress");
        CTX.clearRect(0, 0, WIDTH, HEIGHT);
        ControlCtx.clearRect(0, 0, WIDTH, HEIGHT);
        if (gameData !== null) {
            const _gameCanvas = gameData.canvasArr;
            const IDX = Math.floor(AUDIO.currentTime / SLICE);
            const time = AUDIO.currentTime;
            const map = gameData.map;
            const scoreInfo = gameData.scoreInfo;
            const HIT_RANGE = 0.05;
            const checkHit = (node, controlPosX) => {
                if (_godMode) return true;
                if (controlPosX === -1) return false;
                let d = node.time - time;
                if (Math.abs(d) <= HIT_RANGE) {
                    let rect1 = {x: node._drawInfo.left, width: node._drawInfo.width};
                    let rect2 = {x: controlPosX - CONTROL_RADIUS, width: CONTROL_RADIUS << 1};

                    return rect1.x < rect2.x + rect2.width &&
                        rect1.x + rect1.width > rect2.x;
                }

                return false;
            };
            const shouldHit = (node) => node.time - time <= HIT_RANGE;
            const controlPosXArr = currentPos.map(x => {
                if (x === -1) return -1;
                return x * WIDTH / 100;
            });
            for (let i = 0; i < 2 && i + IDX < _gameCanvas.length; i++) {
                let idx = IDX + i;
                let canvas = _gameCanvas[idx];

                let y = gameData.height - time * PX_PER_SEC - HEIGHT * JUDGE_LINE_POS + canvas._offsetY;
                y |= 0;


                CTX.drawImage(canvas, 0, y, WIDTH, HEIGHT, 0, 0, WIDTH, HEIGHT);

                gameData.notesBin[idx].forEach(j => {
                    let node = map.get(j);

                    if (shouldHit(node) && (!scoreInfo.processedNodes.has(node.id))) {
                        let hit = false;
                        for (let k = controlPosXArr.length - 1; k >= 0; k--) {
                            if (checkHit(node, controlPosXArr[k])) {
                                scoreInfo.hit++;
                                scoreInfo.combo++;
                                hit = true;
                                break;
                            }
                        }

                        if (!hit && time - node.time > HIT_RANGE) {
                            scoreInfo.combo = 0;
                            console.log('Miss');
                            scoreInfo.processedNodes.add(j);
                            setCombo(scoreInfo.combo);
                        } else if (hit) {
                            if (scoreInfo.combo > scoreInfo.maxCombo) {
                                scoreInfo.maxCombo = scoreInfo.combo;
                            }
                            console.log('Hit');
                            scoreInfo.hitNodes.add(j);
                            scoreInfo.processedNodes.add(j);
                            setCombo(scoreInfo.combo);
                        }

                    }

                    if (scoreInfo.hitNodes.has(j)) {
                        let nodeOnScreenY = node._drawInfo.top - y + canvas._offsetY;
                        CTX.clearRect(
                            node._drawInfo.left - SHADOW_SIZE, nodeOnScreenY - SHADOW_SIZE,
                            node._drawInfo.width + SHADOW_SIZE * 2, node._drawInfo.height + SHADOW_SIZE * 2
                        );
                    }
                });
            }

            gameLoop.__progressEle.textContent = getScore().toFixed(2) + '%';
        } //End of gameData!==null

        //Draw Obj
        const JudgeLineY = HEIGHT * JUDGE_LINE_POS;
        ControlCtx.fillStyle = ControlPointColor.fill;
        ControlCtx.lineWidth = 4;
        let startArg = 0;
        let endArg = 2 * Math.PI;
        currentPos.forEach((v, i) => {
            if (v === -1) return;
            ControlCtx.strokeStyle = ControlPointColor[i];
            let x = WIDTH * v / 100;
            ControlCtx.beginPath();
            //ControlCtx.moveTo(x, JudgeLineY);
            ControlCtx.ellipse(x, JudgeLineY, CONTROL_RADIUS, 10, 0, startArg, endArg);
            ControlCtx.fill();
            ControlCtx.stroke();
        });
    }

    requestAnimationFrame(gameLoop);
}

requestAnimationFrame(gameLoop);


function test() {
    Promise.all([
        processData("./assets/json/wingsofpiano.hard.json"),
        testConnect().catch(e => console.error(e)),
    ]).then(dArr => {
        gameData = dArr[0];
        gameData.scoreInfo = {
            hit: 0,
            miss: 0,
            combo: 0,
            maxCombo: 0,
            hitNodes: new Set(),
            processedNodes: new Set(),
        };
        AUDIO.play();
    })


}

let _ws = null;

function testConnect() {
    if (_ws!==null) {
        return new Promise(a => a(_ws));
    }
    return connectToServer().then(ws => {
        _ws = ws;

        ws.onmessage = function (evt) {
            let msg = evt.data;
            
            if(msg instanceof Blob){
                return recResImg(msg);
            }
            
            let data = JSON.parse(msg);
            
            switch(data.op){
                case "move":
                    const handlePosFunc = (idx) => currentPos[idx] = (data.data[idx] !== undefined ? data.data[idx] : -1);
                    handlePosFunc(0);
                    handlePosFunc(1);
                    break;
                
                case "donate":
                    let add = data.data / 10;
                    CONTROL_RADIUS += add;
                    if(CONTROL_RADIUS < 15) CONTROL_RADIUS = 15;
                    break;
            }
            
        };

        ws.onclose = function () {
            currentPos[0] = currentPos[1] = -1;
        };
    });
}

function recResImg(blob){
    let resImg = recResImg.__ele || document.querySelector('#resultImg');
    recResImg.__ele = resImg;
    
    let fr = new FileReader();
    fr.onload = function(){
        resImg.src = this.result;
    };
    fr.readAsDataURL(blob);
}

function getScore() {
    if (gameData) {
        let score = 100 * gameData.scoreInfo.hit / gameData.map.size;
        return score;
    } else {
        return 'N/A';
    }
}

/////////
loadGameData().then(initGame);

/*
$(document.body).on('click', function(){
    if(!document.webkitIsFullScreen){
        this.webkitRequestFullscreen();
    }
});
*/
$(document).on('keydown', function(e){
    e = e.originalEvent;
    if (e.ctrlKey && e.keyCode === 70) {
        document.body.webkitRequestFullscreen();
        e.preventDefault();
    }
});

$('.size-canvas').each((i, c) => {
    c.width = WIDTH;
    c.height = HEIGHT;
});
